module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parser: 'vue-eslint-parser',
  /*
  parserOptions: {
    parser: 'babel-eslint'
  },
  */
  extends: ['prettier', 'plugin:vue/vue3-recommended'],
  plugins: ['prettier'],
  // Custom rule overrides
  rules: {
    'import/order': 'off',
    'new-cap': 'off',
    'prettier/prettier': 'warn',
    'spaced-comment': 'off',
    'standard/no-callback-literal': 'off',
    'vue/html-self-closing': 'off',
    'vue/max-attributes-per-line': 'off',
    'vue/require-v-for-key': 'off'
  }
}
