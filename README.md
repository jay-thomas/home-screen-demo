# Home Screen Demo

[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](LICENSE.md)

### Installation

- Install the node version specified in `.node-version`, preferably using a node package manager such as [fnm](https://github.com/Schniz/fnm)
- Install the [yarn package manager](https://yarnpkg.com/)
- Run the command `yarn` to install dependencies

You can now build a production build with `yarn build` or start webpack-dev-server with `yarn start`.
Run `yarn run` to see additional commands.

## Development

This project uses a bare-bones webpack setup with Vue3's options API enabled.
Initial build will be slow as the favicons are generated but they will be cached for subsequent builds.
