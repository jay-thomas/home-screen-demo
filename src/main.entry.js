import { createApp } from 'vue'
import App from './components/App.vue'
import './main.css'

window.appVersion = APP_VERSION

createApp(App).mount('#app')
