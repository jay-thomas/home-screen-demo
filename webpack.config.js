const appName = require('./package.json').name
const appVersion = require('./package.json').version
const env = process.env.NODE_ENV || 'development'
const eslintWebpackPlugin = require('eslint-webpack-plugin')
const faviconsWebpackPlugin = require('favicons-webpack-plugin')
const htmlWebpackPlugin = require('html-webpack-plugin')
const miniCssExtractPlugin = require('mini-css-extract-plugin')
const path = require('path')
const { VueLoaderPlugin: vueLoaderPlugin } = require('vue-loader')
const webpack = require('webpack')

module.exports = {
  cache: { type: 'filesystem' },
  context: path.join(__dirname, 'src'),
  devServer: {
    historyApiFallback: true,
    port: 8000
  },
  // Using 'source-map' will give the browser the browser
  // the non-transpiled es6 source code for debugging.
  // https://webpack.js.org/configuration/devtool/#devtool
  devtool: 'source-map',
  entry: {
    main: './main.entry.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [miniCssExtractPlugin.loader, 'css-loader']
      },
      {
        exclude: [/node_modules/, /\.spec\.js$/],
        test: /\.js$/,
        use: [
          // Pulls configuration from .swcrc if exists
          { loader: 'swc-loader' }
        ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      }
    ]
  },
  output: {
    clean: true,
    filename: '[name].bundle-[hash].js',
    path: path.join(__dirname, '/dist/')
  },
  plugins: [
    new eslintWebpackPlugin(),
    new faviconsWebpackPlugin({
      logo: './logo.svg',
      hash: true,
      favicons: {
        appName,
        background: '#000',
        theme_color: '#aa4400'
      }
    }),
    new htmlWebpackPlugin({
      title: appName,
      template: './index.ejs'
    }),
    new miniCssExtractPlugin({ filename: '[name].bundle-[hash].css' }),
    new vueLoaderPlugin(),
    new webpack.DefinePlugin({
      APP_VERSION: JSON.stringify(appVersion),
      NODE_ENV: JSON.stringify(env),
      __VUE_OPTIONS_API__: 'true',
      __VUE_PROD_DEVTOOLS__: 'true'
    })
  ],
  resolve: {
    alias: {
      vue: '@vue/runtime-dom'
    }
  }
}
